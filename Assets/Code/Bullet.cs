﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{

    void Start()
    {
        GetComponent<Rigidbody2D>().AddForce(Vector2.up * 200);
        Destroy(this.gameObject, 5);
    }
}
