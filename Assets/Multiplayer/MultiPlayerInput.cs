﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class MultiPlayerInput : MonoBehaviour
{
    private float speed = 10;
    private Rigidbody2D rb;
    public GameObject bulletPrefab;
    private Vector2 move;

    void Awake()
    {
        rb = GetComponent<Rigidbody2D>();

    }

    void Update()
    {
        rb.velocity = move * speed;
    }

    void OnMove(InputValue v)
    {
        move = v.Get<Vector2>();
    }

    void OnFire()
    {
        Instantiate(bulletPrefab, transform.position, Quaternion.identity);
    }
}
