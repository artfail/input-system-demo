﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerNew : MonoBehaviour
{
    private float speed = 10;
    private Rigidbody2D rb;
    public GameObject bulletPrefab;
    private GameController controller;

    void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        controller = new GameController();
        controller.Gameplay.Fire.performed += ctx =>
        {
            OnFire();
            print(ctx);
        };
    }
    public void OnEnable()
    {
        controller.Enable();
    }

    public void OnDisable()
    {
        controller.Disable();
    }

    void Update()
    {
        rb.velocity = controller.Gameplay.Move.ReadValue<Vector2>() * speed;
    }

    void OnFire()
    {
        Instantiate(bulletPrefab, transform.position, Quaternion.identity);
    }
}
