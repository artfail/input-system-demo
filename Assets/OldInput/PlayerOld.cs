﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerOld : MonoBehaviour
{
    private float speed = 10;
    private Rigidbody2D rb;
    public GameObject bulletPrefab;

    void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        float xVelocity = Input.GetAxis("Horizontal");
        float yVelocity = Input.GetAxis("Vertical");
        rb.velocity = new Vector2(xVelocity, yVelocity) * speed;

        if (Input.GetButtonDown("Jump"))
        {
            Instantiate(bulletPrefab, transform.position, Quaternion.identity);
        }
    }
}
